package com.applaudo.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.applaudo.dto.ItemDTO;
import com.applaudo.entities.Item;
import com.applaudo.persistence.ItemDAO;

@Path("/item")
@RequestScoped
public class ItemController {

    @Inject
    private ItemDAO itemDAO;

    @GET
    @Produces(APPLICATION_JSON)
    public List<ItemDTO> getAllItems(){
    	List<Item> items = itemDAO.findAll();
    	List<ItemDTO> itemsDTO = new ArrayList<ItemDTO>();
    	for(Item item : items) {
    		itemsDTO.add(toItemDTO(item));
    	}
        return itemsDTO;
    }

    @GET
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    public Response getItemById(@PathParam("id") int id) {
        Optional<Item> optionalItem = itemDAO.findById(id);
        if(optionalItem.isPresent()){
    		return Response.ok(toItemDTO(optionalItem.get())).build();
    	}else{
    		return Response.status(Response.Status.NOT_FOUND).build();
    	}
    }

    @GET
    @Produces(APPLICATION_JSON)
    public Response getItemByStatusName(@QueryParam("itemStatus") String itemStatus, @QueryParam("itemName") String itemName){
    	List<Item> items = itemDAO.findByStatusName(itemStatus, itemName);
    	 if(items == null || items.isEmpty()){
    		return Response.status(Response.Status.NOT_FOUND).build();
     	}
    	List<ItemDTO> itemsDTO = new ArrayList<ItemDTO>();
    	for(Item item : items) {
    		itemsDTO.add(toItemDTO(item));
    	}
        return Response.ok(itemsDTO).build(); 
    }

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @Transactional
    public Response addItem(ItemDTO itemDTO) {
    	Optional<Item> optionalItem = itemDAO.findById(itemDTO.getItemId());
    	if(optionalItem.isPresent()){
    		return Response.status(Response.Status.BAD_REQUEST).build();
    	}
    	Item item = toItem(itemDTO);
    	itemDAO.insert(item);
    	return Response.status(Response.Status.CREATED).entity(toItemDTO(item)).build();
    }

	private Item toItem(ItemDTO itemDTO) {
		Item item = new Item();
		item.setItemId(itemDTO.getItemId());
		item.setItemName(itemDTO.getItemName());
		item.setItemEnteredByUser(itemDTO.getItemEnteredByUser());
		item.setItemEnteredDate(itemDTO.getItemEnteredDate());
		item.setItemBuyingPrice(itemDTO.getItemBuyingPrice());
		item.setItemSellingPrice(itemDTO.getItemSellingPrice());
		item.setItemLastModifiedByUser(itemDTO.getItemLastModifiedByUser());
		item.setItemLastModifiedDate(itemDTO.getItemLastModifiedDate());
		item.setItemStatus(itemDTO.getItemStatus());
		return item;
	}
	
	private ItemDTO toItemDTO(Item item) {
		ItemDTO itemDTO = new ItemDTO();
		itemDTO.setItemId(item.getItemId());
		itemDTO.setItemName(item.getItemName());
		itemDTO.setItemEnteredByUser(item.getItemEnteredByUser());
		itemDTO.setItemEnteredDate(item.getItemEnteredDate());
		itemDTO.setItemBuyingPrice(item.getItemBuyingPrice());
		itemDTO.setItemSellingPrice(item.getItemSellingPrice());
		itemDTO.setItemLastModifiedByUser(item.getItemLastModifiedByUser());
		itemDTO.setItemLastModifiedDate(item.getItemLastModifiedDate());
		itemDTO.setItemStatus(item.getItemStatus());
		return itemDTO;
	}
}