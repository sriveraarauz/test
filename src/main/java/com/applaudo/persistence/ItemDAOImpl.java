package com.applaudo.persistence;


import java.util.List;
import java.util.Optional;

import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.applaudo.entities.Item;

@Stateful
public class ItemDAOImpl implements ItemDAO {

    @Inject
    private EntityManager em;

    @Context
    UriInfo uriInfo;

    @Override
    public List<Item> findAll() {
        TypedQuery<Item> query = em.createQuery("FROM Item ", Item.class);
        return query.getResultList();
    }

    public Optional<Item> findById(Integer id) {
    	Item item = em.find(Item.class, id);
        return Optional.ofNullable(item);
    }

    @Override
    public void insert(Item item) {
    	em.persist(item);  
    }
    
    @Override
    public List<Item> findByStatusName(String itemStatus, String itemName) {
        TypedQuery<Item> query = em.createQuery("FROM Item i where i.itemStatus = ?1 and LOWER(i.itemName) = LOWER(?2)", Item.class);        
        return query.setParameter(1, itemStatus).setParameter(2, itemName).getResultList();
    }

}