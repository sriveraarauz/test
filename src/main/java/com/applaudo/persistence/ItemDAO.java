package com.applaudo.persistence;

import java.util.List;
import java.util.Optional;

import javax.ejb.Local;

import com.applaudo.entities.Item;

@Local
public interface ItemDAO {

    List<Item> findAll();

    Optional<Item> findById(Integer id);

    void insert(Item item);
    
    List<Item> findByStatusName(String itemStatus, String itemName);
}