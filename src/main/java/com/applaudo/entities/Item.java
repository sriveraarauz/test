package com.applaudo.entities;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Item {

	
private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Integer itemId;
	
	@Column(name = "item_name")
	private String itemName;
	
	@Column(name = "item_entered_by_user")
	private String itemEnteredByUser;
	
	@Column(name = "item_entered_date")
	private Date itemEnteredDate;
	
	@Column(name = "item_buying_price")
	private Double itemBuyingPrice;
	
	@Column(name = "item_selling_price")
	private Double itemSellingPrice;
	
	@Column(name = "item_last_modified_date")
	private Date itemLastModifiedDate;
	
	@Column(name = "item_last_modified_by_user")
	private String itemLastModifiedByUser;
	
	@Column(name = "item_status")
	private String itemStatus;
	

	public Item() {
		super();
	}


	public Integer getItemId() {
		return itemId;
	}


	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getItemEnteredByUser() {
		return itemEnteredByUser;
	}


	public void setItemEnteredByUser(String itemEnteredByUser) {
		this.itemEnteredByUser = itemEnteredByUser;
	}


	public Date getItemEnteredDate() {
		return itemEnteredDate;
	}


	public void setItemEnteredDate(Date itemEnteredDate) {
		this.itemEnteredDate = itemEnteredDate;
	}


	public Double getItemBuyingPrice() {
		return itemBuyingPrice;
	}


	public void setItemBuyingPrice(Double itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}


	public Double getItemSellingPrice() {
		return itemSellingPrice;
	}


	public void setItemSellingPrice(Double itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}


	public Date getItemLastModifiedDate() {
		return itemLastModifiedDate;
	}


	public void setItemLastModifiedDate(Date itemLastModifiedDate) {
		this.itemLastModifiedDate = itemLastModifiedDate;
	}


	public String getItemLastModifiedByUser() {
		return itemLastModifiedByUser;
	}


	public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
		this.itemLastModifiedByUser = itemLastModifiedByUser;
	}


	public String getItemStatus() {
		return itemStatus;
	}


	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
