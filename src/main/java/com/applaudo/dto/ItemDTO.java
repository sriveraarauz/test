package com.applaudo.dto;


import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;


public class ItemDTO {

	
private static final long serialVersionUID = 1L;
	

	private Integer itemId;

	private String itemName;
	
	private String itemEnteredByUser;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Date itemEnteredDate;
	
	private Double itemBuyingPrice;
	
	private Double itemSellingPrice;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Date itemLastModifiedDate;
	
	private String itemLastModifiedByUser;	
	
	private String itemStatus;
	

	public ItemDTO() {
		super();
	}


	public Integer getItemId() {
		return itemId;
	}


	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getItemEnteredByUser() {
		return itemEnteredByUser;
	}


	public void setItemEnteredByUser(String itemEnteredByUser) {
		this.itemEnteredByUser = itemEnteredByUser;
	}


	public Date getItemEnteredDate() {
		return itemEnteredDate;
	}


	public void setItemEnteredDate(Date itemEnteredDate) {
		this.itemEnteredDate = itemEnteredDate;
	}


	public Double getItemBuyingPrice() {
		return itemBuyingPrice;
	}


	public void setItemBuyingPrice(Double itemBuyingPrice) {
		this.itemBuyingPrice = itemBuyingPrice;
	}


	public Double getItemSellingPrice() {
		return itemSellingPrice;
	}


	public void setItemSellingPrice(Double itemSellingPrice) {
		this.itemSellingPrice = itemSellingPrice;
	}


	public Date getItemLastModifiedDate() {
		return itemLastModifiedDate;
	}


	public void setItemLastModifiedDate(Date itemLastModifiedDate) {
		this.itemLastModifiedDate = itemLastModifiedDate;
	}


	public String getItemLastModifiedByUser() {
		return itemLastModifiedByUser;
	}


	public void setItemLastModifiedByUser(String itemLastModifiedByUser) {
		this.itemLastModifiedByUser = itemLastModifiedByUser;
	}


	public String getItemStatus() {
		return itemStatus;
	}


	public void setItemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	


}
