package com.applaudo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URL;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.applaudo.entities.Item;

@RunWith(Arquillian.class)
@RunAsClient
public class ItemTest {
    @Deployment(testable = false)
    public static Archive<?> createTestArchive() {
    	System.out.println("Entro1 ");
       return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Item.class.getPackage())
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource("test-ds.xml");
       
     
    }

   @ArquillianResource
   private URL deploymentUrl;

    @Test
    public void testCRUD() throws Exception {
    	System.out.println("Entro" );
        WebTarget client = ClientBuilder.newClient().target(deploymentUrl.toURI());

    	//WebTarget client = ClientBuilder.newClient().target(new URL("http:/localhost:8080").toURI());
    	
        GenericType<List<Item>> todosListType = new GenericType<List<Item>>() {};
        List<Item> allTodos = client.request().get(todosListType);
        assertEquals(0, allTodos.size());

        Item toDo = new Item();
        toDo.setItemName("My First ToDo");
       // toDo.setOrder(1);
        Item persistedTodo = client.request().post(Entity.entity(toDo, MediaType.APPLICATION_JSON_TYPE), Item.class);
        assertNotNull(persistedTodo.getItemId());

        allTodos = client.request().get(todosListType);
        assertEquals(1, allTodos.size());
        Item fetchedToDo = allTodos.get(0);
        assertEquals(toDo.getItemName(), fetchedToDo.getItemName());

        client.request().delete();

        allTodos = client.request().get(todosListType);
        assertEquals(0, allTodos.size());
    }

}
